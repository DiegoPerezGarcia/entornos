package ejercicio04;

import java.util.Scanner;

public class Ejercicio04 {

	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);
		
		int numHora;
		int numMin;
		int numSec;

		int numSec2;
		int numMin2;
		int numHora2;

		int numDia;
		int numMes;
		int numAnno;

		int numDia2;
		int numMes2;
		int numAnno2;

		int diferencia = 0;
		int error = 0;
		
		do {
			error = 0;
			System.out.println("DAME LA HORA DEL 1er DIA ");
			String hora = scanner.nextLine();

			numHora = Integer.parseInt(hora.substring(0, hora.indexOf(":")));

			numMin = Integer.parseInt(hora.substring((hora.indexOf(":") + 1), hora.lastIndexOf(":")));

			numSec = Integer.parseInt(hora.substring((hora.lastIndexOf(":") + 1), hora.length()));

			if (numHora < 0 || numHora > 23) {
				System.out.println("El formato horario no admite " + numHora + " horas.\n");
				error = 1;
			}

			if (numMin < 0 || numMin > 59) {
				System.out.println("El formato horario no admite " + numMin + " minutos.\n");
				error = 1;
			}

			if (numSec < 0 || numSec > 59) {
				System.out.println("El formato horario no admite " + numSec + " segundos.\n");
				error = 1;
			}

		} while (error == 1);

		// -------------------------------------------------------------------------------------------------------------------------------------------------------

		do {
			error = 0;
			System.out.println("DAME LA 1� FECHA ");
			String fecha = scanner.nextLine();

			numDia = Integer.parseInt(fecha.substring(0, fecha.indexOf("/")));

			numMes = Integer.parseInt(fecha.substring((fecha.indexOf("/") + 1), fecha.lastIndexOf("/")));

			numAnno = Integer.parseInt(fecha.substring((fecha.lastIndexOf("/") + 1), fecha.length()));

			if (numMes == 1 || numMes == 3 || numMes == 5 || numMes == 7 || numMes == 8 || numMes == 10
					|| numMes == 12) {

				if (numDia < 1 || numDia > 31) {

					System.out.println("El formato fecha no admite " + numDia + " dias en este mes.\n");
					error = 1;

				}
			}

			if (numMes == 4 || numMes == 6 || numMes == 9 || numMes == 11 || numMes == 8 || numMes == 10) {

				if (numDia < 1 || numDia > 30) {

					System.out.println("El formato fecha no admite " + numDia + " dias en este mes.\n");
					error = 1;

				}
			}

			if (numMes == 2) {

				if (numAnno % 4 == 0) {
					if (numDia < 1 || numDia > 29) {

						System.out.println("El formato fecha no admite " + numDia + " dias en este mes.\n");
						error = 1;

					}
				} else {
					if (numDia < 1 || numDia > 28) {

						System.out.println("El formato fecha no admite " + numDia + " dias en este mes.\n");
						error = 1;
					}
				}
			}
		} while (error == 1);

		// FIN 1�
		// PARTE--------------------------------------------------------------------

		do {
			error = 0;
			System.out.println("DAME LA HORA DEL 2� DIA ");
			String hora = scanner.nextLine();

			numHora2 = Integer.parseInt(hora.substring(0, hora.indexOf(":")));

			numMin2 = Integer.parseInt(hora.substring((hora.indexOf(":") + 1), hora.lastIndexOf(":")));

			numSec2 = Integer.parseInt(hora.substring((hora.lastIndexOf(":") + 1), hora.length()));

			if (numHora2 < 0 || numHora2 > 23) {
				System.out.println("El formato horario no admite " + numHora2 + " horas.\n");
				error = 1;
			}

			if (numMin2 < 0 || numMin2 > 59) {
				System.out.println("El formato horario no admite " + numMin2 + " minutos.\n");
				error = 1;
			}

			if (numSec2 < 0 || numSec2 > 59) {
				System.out.println("El formato horario no admite " + numSec2 + " segundos.\n");
				error = 1;
			}

		} while (error == 1);

		// -------------------------------------------------------------------------------------------------------------------------------------------------------

		do {
			error = 0;
			System.out.println("DAME LA 2� FECHA (vamos, se que puedes meterla a�n mejor :P)");
			String fecha = scanner.nextLine();

			numDia2 = Integer.parseInt(fecha.substring(0, fecha.indexOf("/")));

			numMes2 = Integer.parseInt(fecha.substring((fecha.indexOf("/") + 1), fecha.lastIndexOf("/")));

			numAnno2 = Integer.parseInt(fecha.substring((fecha.lastIndexOf("/") + 1), fecha.length()));

			if (numMes2 == 1 || numMes2 == 3 || numMes2 == 5 || numMes2 == 7 || numMes2 == 8 || numMes2 == 10
					|| numMes2 == 12) {

				if (numDia2 < 1 || numDia2 > 31) {

					System.out.println("El formato fecha no admite " + numDia2 + " dias en este mes.\n");
					error = 1;

				}
			}

			if (numMes2 == 4 || numMes2 == 6 || numMes2 == 9 || numMes2 == 11 || numMes2 == 8 || numMes2 == 10) {

				if (numDia2 < 1 || numDia2 > 30) {

					System.out.println("El formato fecha no admite " + numDia2 + " dias en este mes.\n");
					error = 1;

				}
			}

			if (numMes2 == 2) {

				if (numAnno2 % 4 == 0) {
					if (numDia2 < 1 || numDia2 > 29) {

						System.out.println("El formato fecha no admite " + numDia2 + " dias en este mes.\n");
						error = 1;

					}
				} else {
					if (numDia2 < 1 || numDia2 > 28) {

						System.out.println("El formato fecha no admite " + numDia2 + " dias en este mes.\n");
						error = 1;
					}
				}
			}
		} while (error == 1);

		scanner.close();
		// FIN 2�
		// PARTE-------------------------------------------------------------------------------------------------------------------------------

		// CONTADOR!!!!-------------------------------------------------------------------------------------------------------------------------------
		while ( numAnno != numAnno2 || numMes != numMes2  ) {
			if (numMes == 1 || numMes == 3 || numMes == 5 || numMes == 7 || numMes == 8 || numMes == 10 || numMes == 12) {
				diferencia = diferencia + 31;
				numMes++;
			} else {
				if (numMes == 4 || numMes == 6 || numMes == 9 || numMes == 11) {
					diferencia = diferencia + 30;
					numMes++;
				}else {
					if(numAnno % 4 == 0 ) {
						diferencia = diferencia + 29;
						numMes++;
					}else {
						diferencia = diferencia + 28;
						numMes++;
					}
				}
			}
			if ( numMes == 13 ) {
				numMes = 1;
				numAnno++;
			}
		}
		
			System.out.println("Hay una diferencia de " + (( diferencia + numDia2) - numDia) + " dias." );
	}

}
